# Welcome

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## SGD213

For SGD213, you will be using this wiki to submit your Assessment 2!
To begin, we've set up a template for you, although you can edit it as much as you like.

Your wiki template begins below.
Make sure that you delete any pages that you don't want to submit - like this welcome page!

We recommend using Visual Studio Code (a separate application to the main Visual Studio we use for Unity) to edit the wiki, as it has a lot of nice features.

### Your Job Now

By the Friday the 16th of April at 5pm...

1. Read the _Editing the Wiki_ section below.
1. Familiarise yourself with the current layout of the wiki.
1. Create your own team Discord server
1. I will post some jobs in your server, read through the job listings, and discuss with your team which job is most befitting for your team's skillset.
1. Create tasks using your Trello tool of choice, and assign them to your team. Make sure to add Arden to your board (asedmak@usc.edu.au)
1. Create sub-pages (and folder structure) as necessary.
1. Fill out the details!

## ! NOTE: Change this structure as much as you like, it is a sample !

## Editing the Wiki

Although you can edit the wiki in the browser...

I recommend that you have a full workflow, because it will give you more practice with git and SourceTree in a reasonably harmless environment:

1. Clone the wiki (not the attached git repo) using git/SourceTree (top right of screen).
    * This is done through the "Wiki" tab in BitBucket
    * Then follow the usual cloning method we are used to
1. Opening the folder using Visual Studio Code or Visual Studio (VS Code is better).

If using VS Code:
1. Installing the following plugins (then restart the editor):
    * Markdown Shortcuts
    * Markdown Theme Kit
    * markdownlint
1. Then you can edit in VS Code and it'll work really nicely!
    * You can also open a preview tab by pressing `CTRL+SHIFT+V`
    * Tabs can be dragged off to a separate panel if desired (drag to right side of screen, usually).
1. Also, you can `git commit`/etc. using VS Code, although I still use SourceTree.
    1. You need to `commit` every time you finish a chunk of work.
    1. When you're happy with your work, `push` it to the server!
    1. (Before doing so, make sure that you `pull` everyone's changes first)

This wiki uses the [Markdown](http://daringfireball.net/projects/markdown/) syntax. The [MarkDownDemo tutorial](https://bitbucket.org/tutorials/markdowndemo) shows how various elements are rendered. Here is a great [cheatsheet for markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).